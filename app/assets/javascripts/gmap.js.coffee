class @Gmap
  this.initMap = ->
    map_options = 
      center:
        lat: -34.397
        lng: 150.644
      zoom: 3
      minZoom: 2
      maxZoom: 20
      draggable: true
      panControl: true
      scrollwheel: true
      
    map = new (google.maps.Map)(document.getElementById('map'), map_options)
    infoWindow = new google.maps.InfoWindow({map: map});

    Gmap.add_panning_button(map)
    
    if navigator.geolocation
      navigator.geolocation.getCurrentPosition ((position) ->
        pos =
          lat: position.coords.latitude
          lng: position.coords.longitude
        
        image = 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=30'
        
        beachMarker = new (google.maps.Marker)(
          position:
            lat: pos.lat
            lng: pos.lng
          map: map
          icon: image
          title: 'Current location'
        )
        
        map.setCenter pos

        infowindow = new google.maps.InfoWindow({
          content: 'your current location'
        })

        beachMarker.addListener('click', () -> 
          infowindow.open(map, beachMarker);
        )
        
        # Add some markers to the map.
        # Note: The code uses the JavaScript Array.prototype.map() method to
        # create an array of markers based on a given "locations" array.
        # The map() method here has nothing to do with the Google Maps API.

        markers = Gmap.getLocations().map((location, i) ->
          Gmap.createMarker(location, i)
        )
        
        # Add a marker clusterer to manage the markers.
        markerCluster = new MarkerClusterer(map, markers,
          imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
          maxZoom: 10
          minZoom: 2
        )

        markerCluster.addListener('clusterclick', () -> 
          console.log('cluster location')
        )

        return
      ), ->
        handleLocationError true, infoWindow, map.getCenter()
        return
    else
      handleLocationError false, infoWindow, map.getCenter()
    return

    handleLocationError = (browserHasGeolocation, infoWindow, pos) ->
      infoWindow.setPosition pos
      infoWindow.setContent if browserHasGeolocation then 'Error: The Geolocation service failed.' else 'Error: Your browser doesn\'t support geolocation.'
      return
  
  this.createMarker = (location, i)->
    marker_window = new google.maps.InfoWindow({
      content: 'pin location'
    })

    # Create an array of alphabetical characters used to label the markers.
    labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    
    marker = new (google.maps.Marker)(
      position: location
      label: labels[i % labels.length])

    marker.addListener('click', () -> 
      marker_window.open(map, marker);
    )
    return marker  

  this.add_panning_button = (map) ->
    toogle_panning_zooming = document.createElement('div');
    centerControl = new Gmap.CenterControl(toogle_panning_zooming, map);
    toogle_panning_zooming.index = 1
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(toogle_panning_zooming);

  this.CenterControl = (controlDiv, map) ->
    controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '2px solid #fff';
    controlUI.style.borderRadius = '3px';
    controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginBottom = '22px';
    controlUI.style.textAlign = 'center';
    controlUI.title = 'Click to disable/enable panning';
    controlDiv.appendChild(controlUI);

    controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '16px';
    controlText.style.lineHeight = '38px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.innerHTML = 'Disable panning';
    controlUI.appendChild(controlText);

    controlUI.addEventListener('click', () -> 
      boolean_value = !map.draggable
      if boolean_value
        controlText.innerHTML = 'Disable panning';
      else
        controlText.innerHTML = 'Enable panning';
      map.setOptions(
        panControl: boolean_value
        zoomControl: boolean_value
        scrollwheel: boolean_value
        overviewMapControl: boolean_value
        keyboardShortcuts: boolean_value
        draggable: boolean_value
      )
    )


  this.getLocations = () ->
    locations = [
      {
        lat: 39.091822
        lng: -104.062500
      }
      {
        lat: 40.404085
        lng: -103.930660
      }
      {
        lat: 39.736168
        lng: -104.999081
      }
      {
        lat: 39.792636
        lng: -104.968802
      }
      {
        lat: 39.788998
        lng: -104.916515
      }
      {
        lat: -33.727111
        lng: 150.371124
      }
      {
        lat: -33.848588
        lng: 151.209834
      }
      {
        lat: -33.851702
        lng: 151.216968
      }
      {
        lat: -34.671264
        lng: 150.863657
      }
      {
        lat: -35.304724
        lng: 148.662905
      }
      {
        lat: -36.817685
        lng: 175.699196
      }
      {
        lat: -36.828611
        lng: 175.790222
      }
      {
        lat: -37.750000
        lng: 145.116667
      }
      {
        lat: -37.759859
        lng: 145.128708
      }
      {
        lat: -37.765015
        lng: 145.133858
      }
      {
        lat: -37.770104
        lng: 145.143299
      }
      {
        lat: -37.773700
        lng: 145.145187
      }
      {
        lat: -37.774785
        lng: 145.137978
      }
      {
        lat: -37.819616
        lng: 144.968119
      }
      {
        lat: -38.330766
        lng: 144.695692
      }
      {
        lat: -39.927193
        lng: 175.053218
      }
      {
        lat: -41.330162
        lng: 174.865694
      }
      {
        lat: -42.734358
        lng: 147.439506
      }
      {
        lat: -42.734358
        lng: 147.501315
      }
      {
        lat: -42.735258
        lng: 147.438000
      }
      {
        lat: -43.999792
        lng: 170.463352
      }
    ]